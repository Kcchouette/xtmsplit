/***************************************************************************
 *   Copyright (C) 2006 by    Bruno Baert and Dorian Baguette              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define XTMS_VERSION "0.2"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <unistd.h>
#include <math.h>

#include "md5.h"

using namespace std;

FILE* lefile = 0;
FILE* lefile2 = 0;

md5_state_t state;
md5_byte_t digest[16];
md5_byte_t* leshash;

char hex_output[16*2 + 1];
int di;

void* buffer;

char header[104];

// Les parametres
bool cut = 1; // par defaut on coupe pas, mais en fait si, pcq ca depend du fichier
int size = 1; // Si on coupe, taille des fichiers en Mbytes (1 par defaut)
int partsize = 0; // Size in bytes of the .xtm files
bool jacadi = 0;
bool md5 = 1;
bool md5_set = 0;
char* fichier_i = 0;
char* fichier_f = 0;
bool fichier_f_set = 0;
char* dir_out = 0;
bool dir_out_set = 0;
bool flb42 = 0;
bool koe = 0; // Keep On Error
bool errors = 0; // Y a-t-il eu des erreurs ?
bool del = 0;
bool silent = 0;


void erreur(char const* message, char const* fichier = nullptr)
{
	printf("(!)ERREUR: %s, filename: [%s].\n", message, fichier);
	if(lefile)
		fclose(lefile);
	if(lefile2)
		fclose(lefile2);
	exit(0);
}

void warning(char const* message)
{
	printf("/!\\Warning: %s\n", message);
}

void show_version()
{
	printf("Xtmsplit version %s\n", XTMS_VERSION);
}

void printhelp()
{
	printf("Xtmsplit %s\n", XTMS_VERSION);
	printf("Usage: xtmsplit [OPTION]... FILENAME...\n");
	printf("Cut files into several .xtm files or put them back together into one file. With no option, a .001.xtm file is required as FILENAME and the file it contains is reconstituted.\n\n");
	printf(" OPTIONS:\n");
	printf(" -cut\t\tcut the files into multiple .xtm files\n");
	printf(" -paste\t\tput the .xtm files back into the original file\n");
	printf(" -md5\t\tforce the use of md5 hashes (which is default behaviour)\n");
	printf(" -nomd5\t\tdon't use md5 hashes\n");
	printf(" -koe\t\tkeep reconstituting the file on md5 errors\n");
	printf(" -d PATH\tdirectory where the reconstituted file will be moved to\n");
	printf(" -f FILENAME\toverwrite the original filename with FILENAME\n");
	printf(" -size n\tspecifies the size of the .xtm files in Mbytes (1024 KBytes)\n");
	printf("\nReport bugs to xtmsplit@caelistis.be\n");
}

void printhelp_fr()
{
	printf("Xtmsplit %s\n", XTMS_VERSION);
	printf("Utilisation: xtmsplit [OPTION]... NOM DE FICHIER...\n");
	printf("Decoupe un fichier en plusieurs fichiers .xtm ou les reassemble en un seul fichier. Si aucune option n'est precisee, un fichier 001.xtm doit etre passe en parametre et le fichier d'origine est reconstitue.\n\n");
	printf(" OPTIONS:\n");
	printf(" -cut\t\t\tdecoupe le fichier en plusieurs fichiers .xtm\n");
	printf(" -paste\t\t\treconstitue le fichier d'origine a partir des fichiers .xtm\n");
	printf(" -md5\t\t\tforce l'utilisation des hash md5 (c'est le comportement par defaut)\n");
	printf(" -nomd5\t\t\tne pas utiliser les hash md5\n");
	printf(" -koe\t\t\tpoursuit l'assemblement des fichiers en cas d'erreur md5\n");
	printf(" -d DOSSIER\t\tdossier dans lequel le fichier reconstitue sera place\n");
	printf(" -f NOM DE FICHIER\tremplace le nom de fichier d'origine par NOM DE FICHIER\n");
	printf(" -size n\t\tspecifie la taille des fichiers .xtm en Mo (1024 Ko)\n");
	printf(" -del\t\t\tsupprime le fichier d'origine en cas de succes de l'operation\n");
	printf(" -silent\t\treduit la quantite de messages affiches en sortie\n");
	printf("\nVous pouvez faire part des bugs ou problemes a xtmsplit@caelistis.be\n");
}

int foutrelebordel() // ne pas appeler cette fonction
{
	if(!(lefile2 = fopen(fichier_i, "wb+")))
		erreur("Impossible de creer un nouveau fichier", fichier_i);
	for(int c=0; c<052; c++)
		fwrite("reponse a la question de la vie, de l'univers, et de tout le reste: 42 (=6*9)", sizeof(char), 78, lefile2);
	fclose(lefile2);
	return 0x2A;
}
		
void couper()
{
	printf("Le fichier a decouper est: \n", fichier_i);
	long long filesize = 0;
	if(! (lefile = fopen(fichier_i,"rb+")))
		erreur("Impossible d'ouvrir le fichier d'origine ", fichier_i);
	fseek(lefile,0,SEEK_END);
	filesize = ftell(lefile);
	char filename[512];
	if(!dir_out_set)
		strcpy(filename, fichier_i);
	else
	{
		strcpy(filename, dir_out);
		int tmp = strlen(filename);
		if(filename[tmp-1] != '/')
		{
			filename[tmp] = '/';
			filename[tmp+1] = 0;
		}
	}
	int c = 0;
	for(c = strlen(filename); c>=0; c--)
	{
		if(filename[c] == '/')
			break;
	}
	filename[c+1] = 0;
	int a = 0;
	int leni = strlen(fichier_i);
	if(!fichier_f_set)
	{
		for(a=leni-1; a>=0; a--)
			if(fichier_i[a] == '/')
				break;
		for(int b=0; b<(leni-a); b++)
		{
			filename[b+c+1] = fichier_i[a+b+1];
		}
		filename[c+leni-a] = 0;
	}
	else
		strcpy(&filename[c+1],fichier_f);
	char orig_file[512];
	strcpy(orig_file,&filename[strlen(filename)-(leni-a)+1]);
	strcat(filename, ".001.xtm");
	if(! (lefile2 = fopen(filename,"wb")))
		erreur("Impossible de creer un nouveau fichier");
	int lu = 0;
	bzero(header,0);
	// Writing the header
	header[0] = strlen("Xtmsplit");
	sprintf(&header[1],"Xtmsplit");
	header[21] = strlen("1.1");
	sprintf(&header[22],"1.1");
	header[40] = strlen(orig_file);
	sprintf(&header[41],orig_file);
	header[91] = md5;
	long int nb = ceil(filesize /(size*1024.0*1024.0));
	memcpy(&header[92], &nb, 4);
	memcpy(&header[96], &filesize, 8);
	if((lu = fwrite(header, sizeof(char), 104, lefile2)) != 104)
		erreur("c'est pas normal");
	//Fin du header
	fseek(lefile2,0,SEEK_SET);
	fread(header, sizeof(char), 104, lefile2);
	fseek(lefile2,104,SEEK_SET);
	fseek(lefile,0,SEEK_SET);
	int bufsize = 65536;
	buffer = malloc(sizeof(char)*bufsize);
	long long geschreven = 0;
	long long writtenpart = 0;
	int nbf = 1;
	partsize = size*1024*1024;
	if(md5)
	{
		leshash = (md5_byte_t*) malloc(sizeof(md5_byte_t)*16*nb);
		md5_init(&state);
		md5_append(&state, (const md5_byte_t *)header, 104);
	}
	while(geschreven < filesize)
	{
		writtenpart = 0;
		while((writtenpart < partsize) && (geschreven < filesize))
		{
				long long taille = fread(buffer, sizeof(char), bufsize, lefile);
				if((geschreven + taille) > filesize)
					taille = filesize - geschreven;
				long long lu = fwrite(buffer, sizeof(char), taille, lefile2);
				if(lu != taille)
					erreur("c'est pas normal");
				geschreven += lu;
				writtenpart += lu;
				if(md5)
					md5_append(&state, (const md5_byte_t *)buffer, lu);
		}
		if(md5)
		{
			md5_finish(&state, digest);
			for(int i=0; i<16; i++)
				*(leshash+(nbf-1)*16+i) = digest[i];
		}
		if(!silent)
			printf("   Creation du fichier %s\n",filename);
		fclose(lefile2);
		lefile2 = 0;
		if(geschreven >= filesize)
			break;
		nbf++;
		if(md5)
		{
			md5_init(&state);
		}
		
		(filename)[strlen(filename)-5] = (nbf%10)+48;
		(filename)[strlen(filename)-6] = (nbf/10)%10+48;
		(filename)[strlen(filename)-7] = (nbf/100)%10+48;
		if(! (lefile2 = fopen(filename,"wb")))
			erreur("Impossible de creer un nouveau fichier");
	}
	if(md5)
	{
		printf("Ajout des hash md5...\n");
		if(! (lefile2 = fopen(filename,"rb+")))
			erreur("Impossible d'ecrire les hash MD5 dans le fichier");
		fseek(lefile2,0,SEEK_END);
		for(int c=0; c< nbf; c++)
		{
			for (di = 0; di < 16; ++di)
				sprintf(hex_output + di * 2, "%02X", *(leshash+c*16+di));
			if((lu = fwrite(hex_output, sizeof(char), 32, lefile2)) != 32)
				erreur("c'est pas normal");
// 			printf("les Hash du fichier %d: %s\n",c+1, hex_output);
		}
	}
	if(lefile2)
		fclose(lefile2);
	if(lefile)
		fclose(lefile);
	if(del)
	{
		printf("Parametre -del active suppression du fichier original: %s\n", fichier_i);
		unlink(fichier_i);
	}
	printf("%s decoupe en %d fichiers de %dMo avec succes\n", fichier_i, nbf, size);
}


void coller()
{
	if(! (lefile = fopen(fichier_i,"rb+")))
		erreur("Impossible d'ouvrir le fichier", fichier_i);
	if(!fread(header, 104, sizeof(char), lefile))
		erreur("Erreur de lecture du header du fichier", fichier_i);
/*
	printf("program name: %s\n", &header[1]);
	printf("version: %s\n", &header[22]);
	printf("original filename: %s\n", &header[41]);
	printf("nombre de fichiers: %u\n", *((int*)&header[92])); // n'importe quoi --- nowhere
	printf("taille du fichier original: %lld\n", *((long long*)&header[96])/1024);
*/
	//checking the version
	if(strcmp(&header[22], "1.1") && strcmp(&header[22], "1.0"))
		erreur("Erreur de version");
	//opening the new file
	char filename[512];
	if(!dir_out_set)
		strcpy(filename, fichier_i);
	else
	{
		strcpy(filename, dir_out);
		int tmp = strlen(filename);
		if(filename[tmp-1] != '/')
		{
			filename[tmp] = '/';
			filename[tmp+1] = 0;
		}
	}
	int c = 0;
	for(c = strlen(filename); c>=0; c--)
		if(filename[c] == '/')
			break;
	if(!fichier_f_set) {
		strncpy(&filename[c+1], &header[41], sizeof(filename)-c-1);
		// according comment l.293-294,
		// filename is between 41 and 92
		// in case of not ending by 0, we create it
		filename[c+1+92-41-1] = '\0';
	}
	else
		strcpy(&filename[c+1], fichier_f);
	if(!(lefile2 = fopen(filename, "wb")))
		erreur("Impossible de creer le fichier", filename);
	int bufsize = 65536;
	long ecrit = 0;
	char path[512];
	strcpy(path, fichier_i);
	buffer = malloc(sizeof(char)*bufsize);
	leshash = (md5_byte_t*) malloc(sizeof(md5_byte_t)*16*(*((int*)&header[92])));
	if(!md5_set)
		md5 = header[91];
	else
	{
		if(md5 && !header[91])
		{
			warning("vous avez specifie -md5 mais le fichier .xtm ne contient pas de hash md5. Verification desactivee.");
		}
	}
	if(md5)
	{
		md5_init(&state);
		md5_append(&state, (const md5_byte_t *)header, 104);
	}
	for(c=1; true;)
	{

		while(!feof(lefile))
		{
			long long taille = fread(buffer, sizeof(char), bufsize, lefile);
			if((ecrit + taille) > *((long long*)&header[96]))
				taille = *((long long*)&header[96]) - ecrit;
			long long lu = fwrite(buffer, sizeof(char), taille, lefile2);
			if(lu != taille)
				erreur("c'est pas normal");
			ecrit += lu;
			if(md5)
				md5_append(&state, (const md5_byte_t *)buffer, lu);
		}
		if(md5)
		{
			md5_finish(&state, digest);
			for(int i=0; i<16; i++)
				*(leshash+(c-1)*16+i) = digest[i];
		}
		if(!silent)
			printf("   Recollage du fichier %d: %s\n",c,path);
		fclose(lefile);
		lefile = 0;
		c++;
		if(c > *((int*)&header[92]))
		{
			break;
		}
		if(md5)
			md5_init(&state);
		(path)[strlen(path)-5] = (c%10)+48;
		(path)[strlen(path)-6] = (c/10)%10+48;
		(path)[strlen(path)-7] = (c/100)%10+48;
		if(!(lefile = fopen(path,"rb+")))
		{
			if(!koe)
				unlink(filename);
			erreur("Impossible d'ouvrir le fichier suivant", path);
		}
	}
	
	if(md5)
	{
		lefile = fopen(path,"rb+");
		fseek(lefile,-32*(*((int*)&header[92])),SEEK_END);
		bool skipmd5 = 0;
		for(int c=0; c<(*((int*)&header[92])); c++)
		{
			fread(buffer, sizeof(char), 32, lefile);
			((char*) (buffer))[32] = 0;
			for (di = 0; di < 16; ++di)
				sprintf(hex_output + di * 2, "%02X", *(leshash+c*16+di));
// 			printf("les Hash du fichier %d: %s - %s\n",c+1, hex_output, buffer);
			if(strcmp(hex_output, (char*)buffer))
			{
				char txt[512];
				sprintf(txt,"hash MD5 du fichier %d incorrect !", c+1);
				printf("Hash md5 calcule: %d - Hash md5 du fichier enregistre: %s\n", c+1, hex_output);
				if(!koe)
				{
					unlink(filename);
					erreur(txt);
				}
				else
				{
					warning(txt);
					skipmd5 = 1;
					errors = 1;
					printf("-koe active: desactivation de la verification MD5 et poursuite du recollage\n");
					break;
				}	
			}
		}
		if(!skipmd5)
			printf("Verification des hash md5 reussie\n");
	}
	if(lefile2)
		fclose(lefile2);
	if(lefile)
		fclose(lefile);
	// Delete the .xtm files if -del parameter activated and no error occured
	if(del && !errors)
	{
		printf("Parametre -del actif: suppression des fichiers .xtm\n");
		for(int i=1; i<= (*((int*)&header[92])); i++)
		{
			(path)[strlen(path)-5] = (i%10)+48;
			(path)[strlen(path)-6] = (i/10)%10+48;
			(path)[strlen(path)-7] = (i/100)%10+48;
			if(!silent)
				printf("   suppression de %s\n", path);
			unlink(path);
		}
	}
	if(!errors)
		printf("%s a ete correctement reconstitue\n", filename);
	else
		printf("%s a ete reconstitue, mais il y a eu des erreurs\n", filename);
}


int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printhelp_fr();
		return 0;
	}
	for(int c=0; c< argc; c++)
	{
// 		printf("argument %i: %s\n", c, argv[c]);
		if(!strcmp(argv[c],"-cut"))
			cut = jacadi = 1;
		else if(!strcmp(argv[c],"-paste"))
		{	cut = 0; jacadi = 1; }
		else if(!strcmp(argv[c],"-md5"))
			{md5 = 1; md5_set = true;}
		else if(!strcmp(argv[c],"-nomd5"))
			{md5 = 0; md5_set = true;}
		else if(!strcmp(argv[c],"-koe"))
			koe = 1;
		else if(!strcmp(argv[c],"-del"))
			del = 1;
		else if(!strcmp(argv[c],"-silent"))
			silent = 1;
		else if(!strcmp(argv[c],"-help"))
			{printhelp_fr(); return 0;}
		else if(!strcmp(argv[c],"--help"))
			{printhelp_fr(); return 0;}
		else if(!strcmp(argv[c],"-flb42"))
			flb42 = !false;
		else if(!strcmp(argv[c],"--version"))
			{show_version(); return 0;}
		else if(!strcmp(argv[c],"-version"))
			{show_version(); return 0;}
		else if(!strcmp(argv[c],"-d"))
		{
			if(c < (argc-1))
			{
				dir_out = argv[++c];
				dir_out_set = true;
			}
			else
				erreur("parametre -d present mais pas de chemin de dossier\n");
		}
		else if(!strcmp(argv[c],"-size"))
		{
			if(c < (argc-1))
				sscanf(argv[++c],"%d\n", &size);
			else
				erreur("parametre -size present mais aucune taille precisee\n");
		}
		else if(!strcmp(argv[c],"-f"))
		{
			if(c < (argc-1))
			{
				fichier_f = argv[++c];
				fichier_f_set = true;
			}
			else
				erreur("parametre -f present mais pas de nom de fichier present\n");
		}
		else
			fichier_i = argv[c];
	}
	if(flb42)
	{
		foutrelebordel();
		return 0;
	}

	if(!fichier_i)
		erreur("nom de fichier necessaire !");
	if(!cut)
		if(strcmp(&fichier_i[strlen(fichier_i)-7], "001.xtm"))
			erreur("Vous devez passer un fichier .001.xtm en parametre\n");
	if(!jacadi)
		if(!strcmp(&fichier_i[strlen(fichier_i)-7], "001.xtm"))
			cut = 0;
// 	printf("cut=%i, md5=%i, jacadi=%i, dir_out=%s, fichier_i=%s\n",cut,md5,jacadi,dir_out,fichier_i);
	printf("Xtmsplit version %s\n", XTMS_VERSION);
	if(!cut)
		coller();
	else
		couper();
	
	return EXIT_SUCCESS;

}
